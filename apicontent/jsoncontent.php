<?php
/**
 * Plugin Name: API Content
 * Plugin URI: https://gitlab.com/alban/wp-apicontent
 * Description: Display remote JSON data using a shortcode
 * Version: 0.1
 * Text Domain: apicontent
 * Author: Alban Bruder
 * Author URI: https://gitlab.com/alban
 */

include 'vendor/autoload.php';

add_filter( 'no_texturize_shortcodes', 'apicontent_no_texturize_shortcodes' );
 
function apicontent_no_texturize_shortcodes( $shortcodes ) {
    $shortcodes[] = 'apicontent';
    return $shortcodes;
}

function apicontent_request($url) {
  $cache_key = "apicontent_" . hash("sha256", $url);
  $body = get_transient($cache_key);

  if (false === $body || isset($_GET['refresh'])) {
    $request = wp_remote_get($url,
      array(
        'timeout'     => 120,
      )
    );

    if(is_wp_error($request)) {
      return false;
    }

    $body = wp_remote_retrieve_body($request);
    set_transient($cache_key, $body, 0);
  }

  $data = json_decode($body);

  if(!is_array($data) && !is_object($data)) {
    return false;
  }

  return $data;
}

function apicontent_shortcode($attrs, $content) {
  $url = $attrs["url"];

  $data = apicontent_request($url);
  if (false === $data) {
    return "";
  }

  $loader = new \Twig\Loader\ArrayLoader([
    'template' => $content,
  ]);
  $twig = new \Twig\Environment($loader);
  $twig->addExtension(new \Twig\Extra\String\StringExtension());

  return apply_shortcodes(wptexturize($twig->render('template', [ 'url' => $url, 'data' => $data ])));
}

add_shortcode('apicontent', 'apicontent_shortcode');